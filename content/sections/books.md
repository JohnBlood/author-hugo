---
title : ""
description: ""
draft: false
weight: 1
---


# If you enjoy hardboiled mysteries, you have come to the right place.

![](/img/benny-cahill-series.jpg "the Benny Cahill series")

The Benny Cahill series is inspired by the detetive and noir shows from the golden age of Radio.