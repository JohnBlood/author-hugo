---
title: "Check Out My First Ebook"
description: "I would like to annouce my first ebook."
date: 2013-12-20T21:22:22-04:00
publishDate: 2013-12-20T21:22:22-04:00
author: "John Paul Wohlscheid"
images: [/../img/dont-gift-horse.png]
draft: false
tags: [ebooks]
---
![](/../img/dont-look-a-gift-horse-in-the-mouth.png "Don't Look a Gift Horse in the Mouth ebook")

I would like to annouce my first ebook. It is the first in a series of stories that follow a private eye named Benny Cahill. Right now it is only available on Smashwords, but It will soon be available through Apple, Sony, Kobo, Barnes and Noble and other ebook retailers. I will post a link to those stores when the book becomes available there. For now, please take a look at the sample on Smashwords and buy it if it interests you. After all its only $0.99. Pass the word and help a hopeful writer.

[https://www.smashwords.com/books/view/388954](https://www.smashwords.com/books/view/388954)