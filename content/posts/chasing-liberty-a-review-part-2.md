---
title: "Chasing Liberty: a Review – Part 2"
description: ""
date: 2015-01-19T12:47:00-04:00
publishDate: 2015-01-19T12:47:00-04:00
author: "John Paul Wohlscheid"
images: [/img/chasing-liberty-cover.jpg]
draft: false
tags: [review]
---

![](/../img/chasing-liberty-cover.jpg "Chasing Liberty book")

*Note: I received an advanced reader's copy of this book from the author for this review.*

This is part two of a two-part review of [Theresa Linden](https://www.theresalinden.com/)'s new book [*Chasing Liberty*](https://www.amazon.com/Chasing-Liberty-Trilogy-Book-ebook/dp/B00GVFF6UW). Part 1 will take a look at the religious aspects of *Chasing Liberty* on [my religion blog](http://realromancatholic.com/2015/01/13/chasing-liberty-a-review-part-1/). Part 2 will examine the overall writing.

And Now for the Weather Report

One of the things that impressed me the most about Theresa's novel is her world-building. As a writer, I know it's not easy to create a believable world for your character to inhabit. Over the course of her story, you really get a sense of the Regimen as an authoritative regime that observes and controls every aspect of their citizens' lives. From characters that Liberty interacts with to the things that she and others have to do, you really get the feeling of Big Brother is looking over their shoulder.

Every story needs a great conflict. For *Chasing Liberty*, Liberty's biggest antagonist is an egotistical doctor named Supero, who becomes fixated on Liberty to the point that he neglects his own job. He is angered by her disinterest in becoming a breeder, by her non-conformity. And when she escapes, he drops everything to find her. He spends more time at the Citizen Security Service looking for her than at his office. (On a side note, whenever Theresa uses the acronym CSS, I chuckle to myself. As a guy who works with webpages, I'm used to thinking of CSS or Cascading Style Sheets. And that is what I think of every time I see CSS).

Overall, *Chasing Liberty* was a very interesting and thought provoking story with an engaging plot and well written main characters. Check it out for yourself [here](https://www.amazon.com/Chasing-Liberty-Trilogy-Book-ebook/dp/B00GVFF6UW).