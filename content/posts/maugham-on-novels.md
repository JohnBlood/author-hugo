---
title: "Maugham on Novels"
description: ""
date: 2014-12-13T11:28:00-04:00
publishDate: 2014-12-13T11:28:00-04:00
author: "John Paul Wohlscheid"
images: [/img/wsomersetmaughamquote.png]
draft: false
tags: [W-Somerset-Maugham, picture, writer-quote]
---

![](/../img/wsomersetmaughamquote.png "Maugham on Novels")