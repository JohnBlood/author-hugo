---
title: "Me First"
description: ""
date: 2014-03-18T22:56:10-04:00
publishDate: 2014-03-18T22:56:10-04:00
author: "John Paul Wohlscheid"
images: [/img/john-d-macdonald.png]
draft: false
tags: [John-D-MacDonald, picture, writer-quote]
---

![Me First](/img/john-d-macdonald.png)

This is exactly how I write. It just makes sense to me. It is easy to write for myself, but writing what other people is alot harder.