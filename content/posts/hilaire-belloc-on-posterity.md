---
title: "Hilaire Belloc on Posterity"
description: ""
date: 2014-09-20T12:27:26-04:00
publishDate: 2014-09-20T12:27:26-04:00
author: "John Paul Wohlscheid"
images: [/img/hilairebellocquote.jpg]
draft: false
tags: [Hilaire-Belloc, picture, writer-quote]
---

![](/img/hilairebellocquote.jpg "Hilaire Belloc on Posterity")