---
title: "A Beginning"
description: ""
date: 2012-10-27T20:56:58-04:00
publishDate: 2012-10-27T20:56:58-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
tags: [introduction]
---

Everything must have a beginning and so this post is the beginning of what I hope to be both educational and enjoyable for my readers.

In my spare time, I like to write short stories. I mostly focus on mysteries, but I’ve also done a couple of westerns, sci-fi and general action stories. I also enjoy writing non-fiction in the topics of religion, technology and history.

I plan to use this blog to pass on writing tricks and tools that I use. I also plan to expound on my philosophy and thoughts on the art and mechanics of writing. I  hope to have guest posts from fellow writers. So stay tuned and enjoy.