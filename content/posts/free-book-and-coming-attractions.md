---
title: "Free Book and Coming Attractions"
description: "Don't Look a Gift Horse in the Mouth is now free on all platforms, except Amazon."
date: 2014-07-24T22:56:10-04:00
publishDate: 2014-07-24T22:56:10-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
tags: [Announcement, Benny-Cahill, ebooks]
---

My ebook *Don't Look a Gift Horse in the Mouth* is now free on all platforms, except Amazon. Unfortunately, I have been unable to find a way to make it free on Amazon. If you enjoy hard-boiled mysteries or detective shows from the golden age of radio, check it out. You can find it on [Smashwords](http://www.smashwords.com/books/view/388954), [Barnes and Noble](http://www.barnesandnoble.com/w/dont-look-a-gift-horse-in-the-mouth-john-wohlscheid/1118020695?ean=2940045525596), [Kobo](http://store.kobobooks.com/en-US/ebook/don-t-look-a-gift-horse-in-the-mouth) or [iTunes](https://itunes.apple.com/us/book/dont-look-gift-horse-in-mouth/id785081066?mt=11). If you like it, feel free to review it.

On another note, I am working on the next installment of the Benny Cahill series. The working title, for now, is *Where There's a Will*. I plan to release it on September 15th. Keep an eye on this blog for more announcements.