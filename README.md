This is the source repository for John Paul Wohlscheid's personal site. You can find it here: https://dazzling-wright-12dd5b.netlify.com/

[![Netlify Status](https://api.netlify.com/api/v1/badges/9d7048bd-3c30-4d24-82d1-b53d887878bb/deploy-status)](https://app.netlify.com/sites/dazzling-wright-12dd5b/deploys)